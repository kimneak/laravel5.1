<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Song;
use App\Article;
use App\Http\Controllers\Session;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
       //  $data = Article::take(4)->get();
       return view('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title'=>'required|unique:article,title']);
        $destinationPath = 'upload';
        
        $add = new Article;
        $add->title   = $request->input("title");
        $add->content = $request->input("content");
        $add->status  = 1;

        if ($request->hasFile('photo')) {
            $file         = $request->file('photo');
            $fileName     = $file->getClientOriginalName();
            $add->image   = $fileName;
            $file->move($destinationPath, $fileName);
        }
        

        $execute = $add->save();
        if ($execute == true) 
            flash()->overlay('Your article has been created', 'Good Job', 'danger');
        
        return redirect("/");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
