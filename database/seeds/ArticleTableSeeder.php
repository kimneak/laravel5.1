<?php

use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('article')->insert([
            'title' => "test",
            'content' => "test",
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 1,
        ]);
    }
}
