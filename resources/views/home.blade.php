
@extends('master')

@section('content')
	@include('flash::message')

	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
  <form method="post" enctype="multipart/form-data">
	  <div class="form-group">
	  	<input type="hidden" name="_method" value="POST">
	  	<input type="hidden" name="_token" value="{{ csrf_token()}}">
	    <label for="exampleInputEmail1">Email address</label>
	    <input type="text" name="title" class="form-control" id="exampleInputEmail1" placeholder="Title">
	  </div>
	  <div class="form-group">
	    <label for="exampleInputPassword1">Password</label>
	    <textarea class="form-control" rows="3" name="content"></textarea>
	  </div>
	  <div class="form-group">
	    <label for="exampleInputPassword1">Password</label>
	   	<input type="file" name="photo">
	  </div>
	  
	  <button type="submit" class="btn btn-default">Submit</button>
	</form>
@stop