<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="{{ asset('bootstrap/css/bootstrap.css') }}" type="text/css">
        <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
        <link href="public/bootstrap/css/bootstrap.css" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>

    </head>
    <body style="margin:10px;">
    <div class="container">
  		
        <div class="row">@yield('content')</div>
       
	</div>
            
        <script type="text/javascript">
        $('#flash-overlay-modal').modal();
        </script>
    </body>
</html>
