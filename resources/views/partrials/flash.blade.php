@if (Session::has('flash_notification.message'))
	<div class="alert alert-success {{ Session::has('message_error')? 'alert-important' : '' }}">
		{{ Session::get('flash_notification.message') }}
	</div>
@endif